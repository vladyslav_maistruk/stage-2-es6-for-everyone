export const controls = {
  PlayerOneAttack: 'KeyA',
  // PlayerOneAttack: ['KeyA'],
  PlayerOneBlock: 'KeyD',
  PlayerTwoAttack: 'KeyJ',
  // PlayerTwoAttack: ['KeyJ'],
  PlayerTwoBlock: 'KeyL',
  // PlayerOneAttackPlayerTwoBlock: ['KeyA', 'KeyL'],
// PlayerTwoAttackPlayerOneBlock: ['KeyL', 'KeyD' ],
  PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],
  PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO']
}